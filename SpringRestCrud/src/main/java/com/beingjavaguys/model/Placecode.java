package com.beingjavaguys.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@Entity
@Table(name = "place_code")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Placecode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "place_Code")
	private long placeCode;

	@Column(name = "place_Name")
	private String placeName;
	
	@ManyToOne
	@JoinColumn(name="City_Code", nullable=false)
	private Citycode citycode;
	
	//Hibernate requires no-args constructor
		public Placecode(){}
		
		public Placecode(long placeCode,String placeName,Citycode c){
			this.placeCode=placeCode;
			this.placeName=placeName;
			this.citycode=c;
		}
	
	public long getPlaceCode() {
		return placeCode;
	}

	public void setPlaceId(long placeCode) {
		this.placeCode = placeCode;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String firstName) {
		this.placeName = placeName;
	}
	public Citycode getCitycode() {
		return citycode;
	}
	public void setCitycode(Citycode cart) {
		this.citycode = cart;
	}
	
	
}

